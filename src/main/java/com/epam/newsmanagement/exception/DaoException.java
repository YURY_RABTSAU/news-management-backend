package com.epam.newsmanagement.exception;

public class DaoException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DaoException() {
		// TODO Auto-generated constructor stub
		super();
	}
	
	public DaoException(String s) {
		// TODO Auto-generated constructor stub
		super(s);
	}
	
	public DaoException(Throwable t) {
		// TODO Auto-generated constructor stub
		super(t);
	}
	
	public DaoException(String s, Throwable t) {
		// TODO Auto-generated constructor stub
		super(s, t);
	}
	
}
