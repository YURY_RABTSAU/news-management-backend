package com.epam.newsmanagement.entity;

import java.util.Date;

/**
 * Class contains info about author
 * @author Yury_Rabtsau
 *
 */
public class AuthorTO extends AbstractTO {

	private String name;
	private Date expired;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getExpired() {
		return expired;
	}

	public void setExpired(Date expired) {
		this.expired = expired;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof AuthorTO)) {
			return false;
		}
		AuthorTO a1 = (AuthorTO) obj;
		boolean compareFlag;
		
		if(name == null) {
			compareFlag = (a1.getName() == null);
		} else {
			compareFlag = name.equals(a1.getName());
		}
		
		if(expired == null) {
			compareFlag = compareFlag && (a1.getExpired() == null);
		} else {
			compareFlag = compareFlag && expired.equals(a1.getExpired());
		}
		
		return this.getId() == a1.getId() && compareFlag;
	}
	
	@Override
	public int hashCode() {
		int hash = 5;
		hash += super.hashCode() * 33;
		hash += ((name == null) ? 0 : name.hashCode()) * 120;
		hash += ((expired == null) ? 0 : expired.hashCode()) * 103;
		return hash;
	}
	
	@Override
	public String toString() {
		StringBuilder res = new StringBuilder(super.toString());
		res.append(", name = ").append(name);
		res.append(", expired = ").append(expired);
		return new String(res);
	}
	
}
