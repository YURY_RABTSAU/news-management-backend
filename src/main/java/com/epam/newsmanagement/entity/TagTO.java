package com.epam.newsmanagement.entity;

/**
 * Class contains info about tag
 * @author Yury_Rabtsau
 *
 */
public class TagTO extends AbstractTO {
	
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof TagTO)) {
			return false;
		}
		TagTO t1 = (TagTO) obj;
		boolean compareFlag;
		if(name == null) {
			compareFlag = (t1.getName() == null);
		} else {
			compareFlag = name.equals(t1.getName());
		}
		return this.getId() == t1.getId() && compareFlag;
	}
	
	@Override
	public int hashCode() {
		int hash = 36;
		hash += super.hashCode() * 108;
		hash += (name == null) ? 0 : name.hashCode() * 25;
		return hash;
	}
	
	@Override
	public String toString() {
		StringBuilder res = new StringBuilder(super.toString());
		res.append(", tag_name = ").append(name);
		return new String(res);
	}

}
