package com.epam.newsmanagement.entity;

import java.util.ArrayList;
import java.util.List;

public class SearchCriteria {
	
	private List<Long> tagIds;
	private long authorId;
	private int offset;
	private int limit;
	
	public SearchCriteria() {
		tagIds = new ArrayList<Long>();
		authorId = -1;
		offset = 0;
		limit = 0;
	}
	
	public boolean isTagSeach() {
		return (tagIds != null) ? !tagIds.isEmpty() : false;
	}
	
	public int getNumberOfTags() {
		if(!isTagSeach()) {
			return 0;
		}
		return tagIds.size();
	}
	
	public boolean isAuthorSearch() {
		return authorId > 0;
	}
	
	public boolean isOffset() {
		return offset > 0;
	}
	
	public boolean isLimited() {
		return limit > 0;
	}

	public List<Long> getTagIds() {
		return tagIds;
	}

	public void setTagIds(List<Long> tagIds) {
		this.tagIds = (tagIds != null) ? tagIds : new ArrayList<Long>();
	}

	public long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(long authorId) {
		this.authorId = authorId;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = (offset<0) ? 0 : offset;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = (limit<0) ? 0 : limit;
	}
	
	public int getOffsetAndLimit() {
		return offset + limit;
	}
	
	public void dropTags() {
		setTagIds(null);
	}
	
	public void dropAuthor() {
		setAuthorId(-1);
	}
	
	public void dropOffset() {
		setOffset(0);
	}
	
	public void dropLimit() {
		setLimit(0);
	}

	@Override
	public String toString() {
		StringBuilder res = new StringBuilder("SearchCriteria: ");
		res.append("tags - ").append(isTagSeach());
		res.append(", author - ").append(isAuthorSearch());
		res.append(", offset - ").append(isOffset());
		res.append(", limited - ").append(isLimited());
		return new String(res);
	}
}
