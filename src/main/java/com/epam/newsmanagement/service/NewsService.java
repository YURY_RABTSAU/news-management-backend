package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.dao.NewsDao;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * Provides low-level service for NewsTO
 * 
 * @author Yury_Rabtsau
 * @version 1.5
 * @since 2015-05-06
 *
 */
public interface NewsService {
	
	/**
	 * Gets NewsTO by its id
	 * @param id - id of the news
	 * @return NewsTO containing found news or null if nothing found
	 * @throws ServiceException
	 * @see {@link NewsDao#getNewsById(long)}
	 */
	public NewsTO getNewsById(long id) throws ServiceException;
	
	/**
	 * Gets news written by the certain author
	 * @param authorId - author's id
	 * @return List containing all found news (empty List if nothing found)
	 * @throws ServiceException
	 * @see {@link NewsDao#getNewsByAuthor(long)}
	 */
	public List<NewsTO> getNewsByAuthor(long authorId) throws ServiceException;
	
	/**
	 * Gets news by tags
	 * @param tagIds - tags' ids
	 * @return List containing all found news (empty List if nothing found)
	 * @throws ServiceException
	 * @see {@link NewsDao#getNewsByTags(List)}
	 */
	public List<NewsTO> getNewsByTags(List<Long> tagIds) throws ServiceException;
	
	/**
	 * Gets news by tags
	 * @param tagIds - tags' ids
	 * @return List containing all found news (empty List if nothing found)
	 * @throws ServiceException
	 * @see {@link #getNewsByTags(List)}
	 */
	public List<NewsTO> getNewsByTags(Long... tagIds) throws ServiceException;
	
	/**
	 * Gets all news in the DB
	 * @return List containing all news (empty List if DB is empty)
	 * @throws ServiceException
	 */
	public List<NewsTO> getNews(List<Long> tagIds, long authorId,
			int offset, int limit) throws ServiceException;
	
	/**
	 * Adds new News to the database
	 * @param news - new peace of news
	 * @return news' id
	 * @throws ServiceException
	 * @see {@link NewsDao#createNews(NewsTO)}
	 */
	public long createNews(NewsTO news) throws ServiceException;
	
	/**
	 * Updates news
	 * @param news - peace of news for updating
	 * @throws ServiceException
	 * @see {@link NewsDao#updateNews(NewsTO)}
	 */
	public void updateNews(NewsTO news) throws ServiceException;
	
	/**
	 * Adds new news or updates existing one
	 * @param news - peace of news for creation/updating
	 * @throws ServiceException
	 * @see {@link NewsDao#createNews(NewsTO)}
	 * @see {@link NewsDao#updateNews(NewsTO)}
	 */
	public void addNews(NewsTO news) throws ServiceException;
	
	/**
	 * Deletes news
	 * @param newsId - id of the news
	 * @throws ServiceException
	 * @see {@link NewsDao#deleteNews(long)}
	 */
	public void deleteNews(long newsId) throws ServiceException;
	
	
}
