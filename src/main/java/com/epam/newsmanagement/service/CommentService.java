package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.dao.CommentDao;
import com.epam.newsmanagement.entity.CommentTO;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * Provides low-level service for CommentTO
 * 
 * @author Yury_Rabtsau
 * @version 1.5
 * @since 2015-05-06
 *
 */
public interface CommentService {

	/**
	 * Gets comment by its id
	 * @param id - comment's id
	 * @return CommentTO containing found comment or null if nothing found
	 * @throws ServiceException
	 * @see {@link CommentDao#getCommentById(long)}
	 */
	public CommentTO getCommentById(long id) throws ServiceException;
		
	/**
	 * Gets comments of the certain news
	 * @param newsId - id of the news
	 * @return List containing all found comments (empty List if nothing found)
	 * @throws ServiceException
	 * @see {@link CommentDao#getCommentsByNews(long)}
	 */
	public List<CommentTO> getCommentsByNews(long newsId) throws ServiceException;
	
	/**
	 * Adds new comment to the database
	 * @param comment - new comment
	 * @return new comment's id
	 * @throws ServiceException
	 * @see {@link CommentDao#createComment(CommentTO)}
	 */
	public long createComment(CommentTO comment) throws ServiceException;
	
	/**
	 * Updates existing comment
	 * @param comment - comment to be updated
	 * @throws ServiceException
	 * @see {@link CommentDao#updateComment(CommentTO)}
	 */
	public void updateComment(CommentTO comment) throws ServiceException;
	
	/**
	 * Adds new comment or updates existing one
	 * @param comment - comment for creation/updating
	 * @throws ServiceException
	 * @see {@link CommentDao#createComment(CommentTO)}
	 * @see {@link CommentDao#updateComment(CommentTO)}
	 */
	public void addComment(CommentTO comment) throws ServiceException;
	
	/**
	 * Deletes comment from the database
	 * @param commentId - comment's id
	 * @throws ServiceException
	 * @see {@link CommentDao#deleteComment(long)}
	 */
	public void deleteComment(long commentId) throws ServiceException;
	
	/**
	 * Adds new comments to the news
	 * 
	 * @param newsId - id of the news
	 * @param comments - comments to add
	 * @throws ServiceException
	 */
	public void addCommentsToNews(long newsId, List<CommentTO> comments) throws ServiceException;
	
	/**
	 * Replaces old comments with new ones
	 * 
	 * @param newsId - id of the news
	 * @param comments - new comments
	 * @throws ServiceException
	 */
	public void changeNewsComments(long newsId, List<CommentTO> comments) throws ServiceException;
	
}
