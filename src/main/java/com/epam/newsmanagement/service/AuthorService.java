package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.dao.AuthorDao;
import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * Provides low-level service for AuthorTO
 * 
 * @author Yury_Rabtsau
 * @version 1.5
 * @since 2015-05-06
 *
 */
public interface AuthorService {

	/**
	 * Gets author by his id
	 * @param id - author's id
	 * @return AuthorTO containing found author or null if nothing found
	 * @throws ServiceException
	 * @see {@link AuthorDao#getAuthorById(long)}
	 */
	public AuthorTO getAuthorById(long id) throws ServiceException;
	
	/**
	 * Gets author by his name
	 * @param name - author's name
	 * @return AuthorTO containing found author or null if nothing found
	 * @throws ServiceException
	 * @see {@link AuthorDao#getAuthorByName(String)}
	 */
	public AuthorTO getAuthorByName(String name) throws ServiceException; 
	
	/**
	 * Gets author of the news
	 * @param newsId - id of the news
	 * @return AuthorTO containing found author or null if nothing found
	 * @throws ServiceException
	 * @see {@link AuthorDao#getAuthorByNews(long)}
	 */
	public AuthorTO getAuthorByNews(long newsId) throws ServiceException;
	
	/**
	 * Finds all authors
	 * @return List containing all authors
	 * @throws ServiceException 
	 * @see {@link AuthorDao#getAllAuthors()}
	 */
	public List<AuthorTO> getAllAuthors() throws ServiceException;
	
	/**
	 * Creates new author in the database
	 * @param author - new author
	 * @return new author's id 
	 * @throws ServiceException
	 * @see {@link AuthorDao#createAuthor(AuthorTO)}
	 */
	public long createAuthor(AuthorTO author) throws ServiceException;
	
	/**
	 * Updates author's data
	 * @param author - author for updating
	 * @throws ServiceException
	 * @see {@link AuthorDao#updateAuthor(AuthorTO)}
	 */
	public void updateAuthor(AuthorTO author) throws ServiceException;
	
	/**
	 * Adds new author or updates existing one
	 * @param author - author for creation/updating
	 * @throws ServiceException
	 * @see {@link AuthorDao#createAuthor(AuthorTO)}
	 * @see {@link AuthorDao#updateAuthor(AuthorTO)}
	 */
	public void addAuthor(AuthorTO author) throws ServiceException;
	
	/**
	 * Deletes author from the database
	 * @param authorId - author's id
	 * @throws ServiceException
	 * @see {@link AuthorDao#deleteAuthor(long)}
	 */
	public void deleteAuthor(long authorId) throws ServiceException;
	
	/**
	 * Connects news and author
	 * @param newsId - id of the news
	 * @param authorId - author's id
	 * @throws ServiceException
	 * @see {@link AuthorDao#createNewsAuthor(long, long)}
	 */
	public void createNewsAuthorRecord(long newsId, long authorId) throws ServiceException;
	
	/**
	 * Disconnects news and author
	 * @param newsId - id of the news
	 * @param authorId - author's id
	 * @throws ServiceException
	 * @see {@link AuthorDao#deleteNewsAuthor(long, long)}
	 */
	public void deleteNewsAuthorRecord(long newsId, long authorId) throws ServiceException;

	/**
	 * Connects Author with the news (and adds Author to the database if
	 * necessary)
	 * 
	 * @param authorId - id of the author of the news
	 * @param newsId - id of the news written by the author
	 * @throws ServiceException
	 */
	public void connectAuthorAndNews(long authorId, long newsId) throws ServiceException;
}
