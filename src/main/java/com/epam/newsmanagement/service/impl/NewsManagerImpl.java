package com.epam.newsmanagement.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.epam.newsmanagement.entity.AbstractTO;
import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.entity.CommentTO;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.NewsManager;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;

/**
 * NewsManager implementation
 * 
 * @author Yury_Rabtsau
 * @version 1.5
 * @since 2015-05-06
 *
 */
public class NewsManagerImpl implements NewsManager{

	private NewsService newsService;
	private AuthorService authorService;
	private CommentService commentService;
	private TagService tagService;

	public NewsManagerImpl() {
	}

	public void setNewsService(NewsService nService) {
		this.newsService = nService;
	}

	public void setAuthorService(AuthorService aService) {
		this.authorService = aService;
	}

	public void setCommentService(CommentService cService) {
		this.commentService = cService;
	}

	public void setTagService(TagService tService) {
		this.tagService = tService;
	}

	@Override
	public NewsVO getNewsById(long id) throws ServiceException {

		NewsVO news;
		NewsTO newsTO = newsService.getNewsById(id);
		if (newsTO == null) {
			return null;
		}
		news = createNewsVOFromTO(newsTO);

		return news;
	}

	@Override
	public List<NewsVO> getNews(List<Long> tagIds, long authorId,
			int offset, int limit) throws ServiceException {

		List<NewsVO> resultList = new ArrayList<NewsVO>();
		List<NewsTO> listTO = newsService.getNews(tagIds, authorId, offset, limit);
		for (NewsTO newsTO : listTO) {
			resultList.add(createNewsVOFromTO(newsTO));
		}

		return resultList;
	}

	@Override
	public List<NewsVO> getNewsByAuthor(AuthorTO author) throws ServiceException {

		List<NewsVO> resultList = new ArrayList<NewsVO>();
		List<NewsTO> listTO = newsService.getNewsByAuthor(author.getId());
		for (NewsTO newsTO : listTO) {
			resultList.add(createNewsVOFromTO(newsTO));
		}

		return resultList;
	}

	@Override
	public List<NewsVO> getNewsByTag(TagTO tag) throws ServiceException {

		List<NewsVO> resultList = new ArrayList<NewsVO>();
		List<Long> idList = new ArrayList<Long>();
		idList.add(tag.getId());
		List<NewsTO> listTO = newsService.getNewsByTags(idList);
		for (NewsTO newsTO : listTO) {
			resultList.add(createNewsVOFromTO(newsTO));
		}

		return resultList;
	}

	@Override
	public List<NewsVO> getNewsByTags(List<TagTO> tags) throws ServiceException {

		List<NewsVO> resultVOList = new ArrayList<NewsVO>();
		
		List<Long> tagIds = new ArrayList<Long>();
		for (TagTO tagTO : tags) {
			tagIds.add(tagTO.getId());
		}

		List<NewsTO> resultTOList = newsService.getNewsByTags(tagIds);
		for (NewsTO newsTO : resultTOList) {
			resultVOList.add(createNewsVOFromTO(newsTO));
		}

		return resultVOList;
	}

	@Override
	public void addNews(NewsVO news) throws ServiceException {

		newsService.addNews(news.getNewsTO());
		authorService.connectAuthorAndNews(news.getAuthor().getId(), news.getNewsTO().getId());
		commentService.changeNewsComments(news.getId(), news.getComments());
		tagService.changeNewsTags(news.getId(), getIdsFromEntityList(news.getTags()));
	}

	private NewsVO createNewsVOFromTO(NewsTO newsTO) throws ServiceException {

		List<CommentTO> commentList = commentService.getCommentsByNews(newsTO.getId());
		List<TagTO> tagList = tagService.getTagsByNews(newsTO.getId());
		AuthorTO author = authorService.getAuthorByNews(newsTO.getId());

		NewsVO newsVO = new NewsVO();
		newsVO.setNewsTO(newsTO);
		newsVO.setAuthor(author);
		newsVO.setComments(commentList);
		newsVO.setTags(tagList);

		return newsVO;
	}
	
	private List<Long> getIdsFromEntityList(List<? extends AbstractTO> entityList) {
		List<Long> idList = new ArrayList<Long>(entityList.size());
		for(AbstractTO entity : entityList) {
			idList.add(entity.getId());
		}
		return idList;
	}

}
