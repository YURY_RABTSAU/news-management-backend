package com.epam.newsmanagement.service.impl;

import java.util.List;

import com.epam.newsmanagement.dao.CommentDao;
import com.epam.newsmanagement.entity.CommentTO;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.CommentService;

/**
 * CommentService implementation
 * 
 * @author Yury_Rabtsau
 * @version 1.5
 * @since 2015-05-06
 *
 */
public class CommentServiceImpl implements CommentService {
	
	private CommentDao commentDao;
	
	public CommentServiceImpl() {}
	
	public CommentServiceImpl(CommentDao commentDao) {
		this.setCommentDao(commentDao);
	}

	public void setCommentDao(CommentDao commentDao) {
		this.commentDao = commentDao;
	}
	
	@Override
	public CommentTO getCommentById(long id) throws ServiceException {
		try {
			return commentDao.getCommentById(id);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}
	
	@Override
	public List<CommentTO> getCommentsByNews(long newsId) throws ServiceException {
		try {
			return commentDao.getCommentsByNews(newsId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}
	
	@Override
	public long createComment(CommentTO comment) throws ServiceException {
		try {
			return commentDao.createComment(comment);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}
	
	@Override
	public void updateComment(CommentTO comment) throws ServiceException {
		try {
			commentDao.updateComment(comment);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}
	
	@Override
	public void addComment(CommentTO comment) throws ServiceException {
		try {
			CommentTO commentCheck = commentDao.getCommentById(comment.getId());
			if (commentCheck == null) {
				commentDao.createComment(comment);
			} else {
				commentDao.updateComment(comment);
			}
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}
	
	@Override
	public void deleteComment(long commentId) throws ServiceException {
		try {
			commentDao.deleteComment(commentId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}
	
	@Override
	public void addCommentsToNews(long newsId, List<CommentTO> comments)
			throws ServiceException {
		if(newsId <= 0 || comments == null) {
			return;
		}
		for (CommentTO commentTO : comments) {
			commentTO.setNewsId(newsId);
			addComment(commentTO);
		}
	}

	@Override
	public void changeNewsComments(long newsId, List<CommentTO> comments)
			throws ServiceException {
		
		List<CommentTO> currentComments = getCommentsByNews(newsId);
		for (CommentTO commentTO : currentComments) {
			deleteComment(commentTO.getId());
		}
		addCommentsToNews(newsId, comments);
	}

}
