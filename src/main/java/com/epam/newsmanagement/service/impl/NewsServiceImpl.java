package com.epam.newsmanagement.service.impl;

import java.util.Arrays;
import java.util.List;

import com.epam.newsmanagement.dao.NewsDao;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsService;

/**
 * NewsService implementation
 * 
 * @author Yury_Rabtsau
 * @version 1.5
 * @since 2015-05-06
 *
 */
public class NewsServiceImpl implements NewsService {

	private NewsDao newsDao;
	
	public NewsServiceImpl() {}
	
	public NewsServiceImpl(NewsDao newsDao) {
		this.setNewsDao(newsDao);
	}

	public void setNewsDao(NewsDao newsDao) {
		this.newsDao = newsDao;
	}

	@Override
	public NewsTO getNewsById(long id) throws ServiceException {
		try {
			return newsDao.getNewsById(id);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<NewsTO> getNewsByAuthor(long authorId) throws ServiceException {
		try {
			return newsDao.getNewsByAuthor(authorId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<NewsTO> getNewsByTags(List<Long> tagIds) throws ServiceException {
		try {
			return newsDao.getNewsByTags(tagIds);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}
	
	@Override
	public List<NewsTO> getNewsByTags(Long... tagIds) throws ServiceException {
		List<Long> idList = Arrays.asList(tagIds);
		return getNewsByTags(idList);
	}

	@Override
	public List<NewsTO> getNews(List<Long> tagIds, long authorId,
			int offset, int limit) throws ServiceException {
		try {
			return newsDao.getNews(tagIds, authorId, offset, limit);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public long createNews(NewsTO news) throws ServiceException {
		try {
			return newsDao.createNews(news);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void updateNews(NewsTO news) throws ServiceException {
		try {
			newsDao.updateNews(news);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void addNews(NewsTO news) throws ServiceException {
		try {
			NewsTO newsCheck = newsDao.getNewsById(news.getId());
			if (newsCheck == null) {
				newsDao.createNews(news);
			} else {
				newsDao.updateNews(news);
			}
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void deleteNews(long newsId) throws ServiceException {
		try {
			newsDao.deleteNews(newsId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

}
