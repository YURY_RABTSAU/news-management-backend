package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.exception.DaoException;

/**
 * Interface for database access to Author
 * @author Yury_Rabtsau
 * @version 1.5
 * @since 2015-05-06
 *
 */
public interface AuthorDao {

	/**
	 * Finds the author by his id
	 * @param id - author's id
	 * @return AuthorTO containing found author or null if nothing found
	 * @throws DaoException if impossible to complete the operation
	 */
	public AuthorTO getAuthorById(long id) throws DaoException;
	
	/**
	 * Finds the author by his name
	 * @param name - author's name
	 * @return AuthorTO containing found author or null if nothing found
	 * @throws DaoException if impossible to complete the operation
	 */
	public AuthorTO getAuthorByName(String name) throws DaoException;
	
	/**
	 * Finds the author of the certain news
	 * @param newsId - news id
	 * @return AuthorTO containing found author or null if nothing found
	 * @throws DaoException if impossible to complete the operation
	 */
	public AuthorTO getAuthorByNews(long newsId) throws DaoException;
	
	/**
	 * Finds all authors
	 * @return List containing all authors
	 * @throws DaoException if impossible to complete the operation
	 */
	public List<AuthorTO> getAllAuthors() throws DaoException;
	
	/**
	 * Inserts new author record into database
	 * @param author - data to be inserted, his id will be changed
	 * @return id of the inserted author
	 * @throws DaoException if impossible to complete the operation
	 */
	public long createAuthor(AuthorTO author) throws DaoException;
	
	/**
	 * Updates author record in the database
	 * @param author - data to be updated
	 * @throws DaoException if impossible to complete the operation
	 */
	public void updateAuthor(AuthorTO author) throws DaoException;
	
	/**
	 * Deletes author from the database
	 * @param authorId - id of the record to be deleted
	 * @throws DaoException if impossible to complete the operation
	 */
	public void deleteAuthor(long authorId) throws DaoException;
	
	/**
	 * Inserts new news-author record into database
	 * @param newsId - id of the news
	 * @param authorId - author's id
	 * @throws DaoException if impossible to complete the operation
	 */
	public void createNewsAuthor(long newsId, long authorId) throws DaoException;
	
	/**
	 * Deletes news-author record from the database
	 * @param newsId - id of the news
	 * @param authorId - author's id
	 * @throws DaoException if impossible to complete the operation
	 */
	public void deleteNewsAuthor(long newsId, long authorId) throws DaoException;
	
}
