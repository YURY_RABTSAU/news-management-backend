package com.epam.newsmanagement.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

/**
 * Utility class for closing JDBC resources
 * @author Yury_Rabtsau
 * @version 1.1
 * @since 2015-05-06
 *
 */
public class JdbcUtils {
	
	private static final Logger LOG = Logger.getLogger(JdbcUtils.class);
	
	/**
	 * Closes Connection, PreparedStatement, ResultSet
	 * @param con - Connection to be closed
	 * @param ps - PreparedStatement to be closed
	 * @param rs - ResultSet to be closed
	 */
	public static void closeResources(Connection con, PreparedStatement ps, ResultSet rs) {
		closeResultSet(rs);
		closeResources(con, ps);
	}

	/**
	 * Closes Connection, PreparedStatement
	 * @param con - Connection to be closed
	 * @param ps - PreparedStatement to be closed
	 */
	public static void closeResources(Connection con, PreparedStatement ps) {
		closePreparedStatement(ps);
		closeConnection(con);		
	}
	
	/**
	 * Closes Connection, Statement, ResultSet
	 * @param con - Connection to be closed
	 * @param st - Statement to be closed
	 * @param rs - ResultSet to be closed
	 */
	public static void closeResources(Connection con, Statement st, ResultSet rs) {
		closeResultSet(rs);
		closeStatement(st);
		closeConnection(con);
	}
	
	/**
	 * Closes ResultSet
	 * @param rs - ResultSet to be closed
	 */
	public static void closeResultSet(ResultSet rs) {
		try {
			if (rs != null) {
				rs.close();
			}
		} catch (SQLException e) {
			LOG.warn("can't close resources (ResultSet) properly", e);
		}
	}
	
	/**
	 * Closes PreparedStatement
	 * @param ps - PreparedStatement to be closed
	 */
	public static void closePreparedStatement(PreparedStatement ps) {
		try {
			if (ps != null) {
				ps.close();
			}
		} catch (SQLException e) {
			LOG.warn("can't close resources (PreparedStatement) properly", e);
		}
	}
	
	/**
	 * Closes Statement
	 * @param st - Statement to be closed
	 */
	public static void closeStatement(Statement st) {
		try {
			if (st != null) {
				st.close();
			}
		} catch (SQLException e) {
			LOG.warn("can't close resources (Statement) properly", e);
		}
	}
	
	/**
	 * Closes Connection
	 * @param con - Connection to be closed
	 */
	public static void closeConnection(Connection con) {
		try {
			if (con != null) {
				con.close();
			}
		} catch (SQLException e) {
			LOG.warn("can't close resources (Connection) properly", e);
		}
	}

}
