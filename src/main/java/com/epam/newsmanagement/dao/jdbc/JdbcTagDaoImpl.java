package com.epam.newsmanagement.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.epam.newsmanagement.dao.TagDao;
import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.exception.DaoException;

/**
 * JDBC implementation of TagDao 
 * 
 * @author Yury_Rabtsau
 * @version 1.5
 * @since 2015-05-06
 *
 */
public class JdbcTagDaoImpl implements TagDao {

	private static Logger LOG = Logger.getLogger(JdbcTagDaoImpl.class); 
	private DataSource dataSource;
	
	private static final String SQL_GET_BY_ID = 
			"select tag_id, tag_name from tag where tag_id=? ";
	private static final String SQL_GET_BY_NAME = 
			"select tag_id, tag_name from tag where tag_name=? ";
	private static final String SQL_GET_BY_NEWS = 
			"select tag.tag_id, tag_name from tag join news_tag on tag.tag_id=news_tag.tag_id where news_id=? ";
	private static final String SQL_GET_ALL_TAGS = 
			"select tag_id, tag_name from tag";
	private static final String SQL_CREATE_TAG = 
			"insert into tag (tag_id, tag_name) values (TAG_ID_SEQ.NEXTVAL, ?) ";
	private static final String SQL_UPDATE_TAG = 
			"update tag set tag_name=? where tag_id=? ";
	private static final String SQL_DELETE_TAG = 
			"delete from tag where tag_id=? ";
	private static final String SQL_CREATE_NEWS_TAG = 
			"insert into news_tag (tag_id, news_id) values (?, ?) ";
	private static final String SQL_DELETE_NEWS_TAG = 
			"delete from news_tag where tag_id=? and news_id=? ";
	
	private static final String COLUMN_TAG_ID = "tag_id";
	private static final String COLUMN_TAG_NAME = "tag_name";

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	@Override
	public TagTO getTagById(long id) throws DaoException {
		
		TagTO result = null;
		LOG.debug("getTagById start, id: " + id);
		
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(SQL_GET_BY_ID);
			ps.setLong(1, id);
			rs = ps.executeQuery();
			LOG.debug("execute statement");
			if(rs.next()) {
				LOG.debug("resultSet is not empty");
				result = parseResultSetRow(rs);
			}
			
		} catch (SQLException e) {
			LOG.warn("SQLException in method getTagById", e);
			throw new DaoException("SQLException", e);
		} finally {
			JdbcUtils.closeResources(con, ps, rs);
		}
		
		LOG.debug("getTagById end");
		return result;
	}

	@Override
	public TagTO getTagByName(String name) throws DaoException {

		TagTO result = null;
		LOG.debug("getTagByName start, tag name: " + name);
		
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(SQL_GET_BY_NAME);
			ps.setString(1, name);
			rs = ps.executeQuery();
			LOG.debug("execute statement");
			if(rs.next()) {
				LOG.debug("resultSet is not empty");
				result = parseResultSetRow(rs);
			}
			
		} catch (SQLException e) {
			LOG.warn("SQLException in method getTagByName", e);
			throw new DaoException("SQLException", e);
		} finally {
			JdbcUtils.closeResources(con, ps, rs);
		}
		
		LOG.debug("getTagByName end");
		return result;
	}

	@Override
	public List<TagTO> getTagsByNews(long newsId) throws DaoException {
		
		LOG.debug("getTagsByNews start, news id: " + newsId);
		List<TagTO> result = new ArrayList<TagTO>();
		
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(SQL_GET_BY_NEWS);
			ps.setLong(1, newsId);
			rs = ps.executeQuery();
			LOG.debug("execute statement");
			while(rs.next()) {
				TagTO tag = parseResultSetRow(rs);
				result.add(tag);
			}
			
		} catch (SQLException e) {
			LOG.warn("SQLException in method getTagsByNews", e);
			throw new DaoException("SQLException", e);
		} finally {
			JdbcUtils.closeResources(con, ps, rs);
		}
		
		LOG.debug("getTagsByNews end");
		return result;
	}
	
	@Override
	public List<TagTO> getAllTags() throws DaoException {
		
		LOG.debug("getAllTags start");
		List<TagTO> resultList = new ArrayList<TagTO>();
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			con = dataSource.getConnection();
			st = con.createStatement();
			rs = st.executeQuery(SQL_GET_ALL_TAGS);
			while(rs.next()) {
				TagTO tag = parseResultSetRow(rs);
				resultList.add(tag);
			}
		} catch (SQLException e) {
			LOG.warn("SQLException in method getAllTags", e);
			throw new DaoException("SQLException", e);
		} finally {
			JdbcUtils.closeResources(con, st, rs);
		}
		LOG.debug("getAllTags end");
		return resultList;
	};

	@Override
	public long createTag(TagTO tag) throws DaoException {
		
		LOG.debug("createTag start");
		long newId = -1;
		
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rsId = null;
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(SQL_CREATE_TAG,
					new String[] {COLUMN_TAG_ID});
			
			ps.setString(1, tag.getName());
			
			ps.executeUpdate();
			LOG.debug("execute statement");
			
			rsId = ps.getGeneratedKeys();
			if(rsId.next()) {
				newId = rsId.getLong(1);
				tag.setId(newId);
			}
			
		} catch (SQLException e) {
			LOG.warn("SQLException in method createTag", e);
			throw new DaoException("SQLException", e);
		} finally {
			JdbcUtils.closeResources(con, ps, rsId);
		}
		
		LOG.debug("createTag end, id=" + newId);
		return newId;
		
	}

	@Override
	public void updateTag(TagTO tag) throws DaoException {
		
		LOG.debug("updateTag start");
		
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(SQL_UPDATE_TAG);
			ps.setString(1, tag.getName());
			ps.setLong(2, tag.getId());
			
			ps.executeUpdate();
			LOG.debug("execute statement");
			
		} catch (SQLException e) {
			LOG.warn("SQLException in method updateTag", e);
			throw new DaoException("SQLException", e);
		} finally {
			JdbcUtils.closeResources(con, ps);
		}
		
		LOG.debug("updateTag end");
		
	}

	@Override
	public void deleteTag(long tagId) throws DaoException {
		
		LOG.debug("deleteTag start, id=" + tagId);
		
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(SQL_DELETE_TAG);
			ps.setLong(1, tagId);
			
			ps.executeUpdate();
			LOG.debug("execute statement");
			
		} catch (SQLException e) {
			LOG.warn("SQLException in method deleteTag", e);
			throw new DaoException("SQLException", e);
		} finally {
			JdbcUtils.closeResources(con, ps);
		}
		
		LOG.debug("deleteTag end");
		
	}

	@Override
	public void createNewsTag(long newsId, long tagId) throws DaoException {
		
		LOG.debug("createNewsTag start");
		
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(SQL_CREATE_NEWS_TAG);
			ps.setLong(1, tagId);
			ps.setLong(2, newsId);
			
			ps.executeUpdate();
			LOG.debug("execute statement");
			
		} catch (SQLException e) {
			LOG.warn("SQLException in method createNewsTag", e);
			throw new DaoException("SQLException", e);
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				LOG.warn("can't close resources properly", e);
			}
		}
		
		LOG.debug("createNewsTag end");
		
	}



	@Override
	public void deleteNewsTag(long newsId, long tagId) throws DaoException {
		
		LOG.debug("deleteNewsTag start");
		
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(SQL_DELETE_NEWS_TAG);
			ps.setLong(1, tagId);
			ps.setLong(2, newsId);
			
			ps.executeUpdate();
			LOG.debug("execute statement");
			
		} catch (SQLException e) {
			LOG.warn("SQLException in method deleteNewsTag", e);
			throw new DaoException("SQLException", e);
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				LOG.warn("can't close resources properly", e);
			}
		}
		
		LOG.debug("deleteNewsTag end");
		
	}
	
	private TagTO parseResultSetRow(ResultSet rs) throws SQLException {
		TagTO tag = new TagTO();
		tag.setId(rs.getLong(COLUMN_TAG_ID));
		tag.setName(rs.getString(COLUMN_TAG_NAME));
		return tag;
	}

}
