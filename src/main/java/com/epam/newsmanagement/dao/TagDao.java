package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.exception.DaoException;

/**
 * Interface for database access to Tag
 * @author Yury_Rabtsau
 * @version 1.5
 * @since 2015-05-06
 *
 */
public interface TagDao {

	/**
	 * Finds the tag by its id
	 * @param id - tag's id
	 * @return TagTO containing found tag or null if nothing found
	 * @throws DaoException if impossible to complete the operation
	 */
	public TagTO getTagById(long id) throws DaoException;
	
	/**
	 * Finds the tag by its name
	 * @param name - tag's name
	 * @return TagTO containing found tag or null if nothing found
	 * @throws DaoException if impossible to complete the operation
	 */
	public TagTO getTagByName(String name) throws DaoException;
	
	/**
	 * Finds tags for the certain news
	 * @param newsId - id of the news
	 * @return List containing all found tags (empty List if nothing found)
	 * @throws DaoException if impossible to complete the operation
	 */
	public List<TagTO> getTagsByNews(long newsId) throws DaoException;
	
	/**
	 * Finds all tags
	 * @return List containing all tags
	 * @throws DaoException if impossible to complete the operation
	 */
	public List<TagTO> getAllTags() throws DaoException;
	
	/**
	 * Inserts new tag record into database
	 * @param tag - data to be inserted, its id will be changed
	 * @return id of the inserted tag
	 * @throws DaoException if impossible to complete the operation
	 */
	public long createTag(TagTO tag) throws DaoException;
	
	/**
	 * Updates tag record in the database
	 * @param tag - data to be updated
	 * @throws DaoException if impossible to complete the operation
	 */
	public void updateTag(TagTO tag) throws DaoException;
	
	/**
	 * Deletes tag from the database
	 * @param tagId - id of the record to be deleted
	 * @throws DaoException if impossible to complete the operation
	 */
	public void deleteTag(long tagId) throws DaoException;
	
	/**
	 * Inserts new news-tag record into database
	 * @param newsId - id of the news
	 * @param tagId - tag's id
	 * @throws DaoException if impossible to complete the operation
	 */
	public void createNewsTag(long newsId, long tagId) throws DaoException;
	
	//public void updateNewsTag(long newsId, long tagId) throws DaoException;
	
	/**
	 * Deletes news-tag record from the database
	 * @param newsId - id of the news
	 * @param tagId - tag's id
	 * @throws DaoException if impossible to complete the operation
	 */
	public void deleteNewsTag(long newsId, long tagId) throws DaoException;
	
}
