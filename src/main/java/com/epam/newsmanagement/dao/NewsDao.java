package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.DaoException;

/**
 * Interface for database access to News
 * @author Yury_Rabtsau
 * @version 1.5
 * @since 2015-05-06
 *
 */
public interface NewsDao {

	/**
	 * Finds the news by its id
	 * @param id - news id
	 * @return NewsTO containing found news or null if nothing found
	 * @throws DaoException if impossible to complete the operation
	 */
	public NewsTO getNewsById(long id) throws DaoException;
	
	/**
	 * Finds news written by the certain author
	 * @param authorId - author's id
	 * @return List containing all found news (empty List if nothing found)
	 * @throws DaoException if impossible to complete the operation
	 */
	public List<NewsTO> getNewsByAuthor(long authorId) throws DaoException;
	
	/**
	 * Finds news with the certain tags
	 * @param tagIds - tags' ids
	 * @return List containing all found news (empty List if nothing found)
	 * @throws DaoException if impossible to complete the operation
	 */
	public List<NewsTO> getNewsByTags(List<Long> tagIds) throws DaoException;
	
	/**
	 * Finds all news according to given conditions
	 * @param tagIds - tags' ids
	 * @param authorId - author's id
	 * @param offset - how many peaces of news to skip
	 * @param limit - maximum number of peaces of news to get
	 * @return List containing all news (empty List if nothing found)
	 * @throws DaoException if impossible to complete the operation
	 */
	public List<NewsTO> getNews(List<Long> tagIds, long authorId,
			int offset, int limit) throws DaoException;
	
	/**
	 * Finds all news according to given conditions
	 * @param searchCriteria - contains conditions of the search
	 * @return List containing all news (empty List if nothing found)
	 * @throws DaoException if impossible to complete the operation
	 */
	public List<NewsTO> getNews(SearchCriteria searchCriteria) throws DaoException;
	
	/**
	 * Inserts new news record into database
	 * @param news - data to be inserted, its id will be changed
	 * @return id of the inserted news
	 * @throws DaoException if impossible to complete the operation
	 */
	public long createNews(NewsTO news) throws DaoException;
	
	/**
	 * Updates news record in database
	 * @param news - data to be updated
	 * @throws DaoException if impossible to complete the operation
	 */
	public void updateNews(NewsTO news) throws DaoException;
	
	/**
	 * Deletes news from the database
	 * @param newsId - id of the record to be deleted
	 * @throws DaoException if impossible to complete the operation
	 */
	public void deleteNews(long newsId) throws DaoException;
	
	
	
}
