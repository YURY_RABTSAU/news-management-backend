package com.epam.newsmanagement.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.epam.newsmanagement.dal.dao.jdbc.DateParseUtils;
import com.epam.newsmanagement.dao.NewsDao;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.service.impl.NewsServiceImpl;

public class NewsServiceTest {

	private NewsServiceImpl nService;
	@Mock
	private NewsDao newsDao;

	@Before
	public void beforeTest() {
		MockitoAnnotations.initMocks(this);
		nService = new NewsServiceImpl();
		nService.setNewsDao(newsDao);
	}

	@Test
	public void testGetNewsById() throws Exception {
		NewsTO news = new NewsTO();
		news.setId(4L);
		news.setTitle("title");
		news.setShortText("bla-bla");
		news.setFullText("bla-bla-bla-bla-bla");
		news.setCreationDate(DateParseUtils
				.getTimestampDateFromString("2015-01-22 12:54:12.851"));
		news.setModificationDate(DateParseUtils
				.getSimpleDateFromString("2015-03-11"));
		when(newsDao.getNewsById(4L)).thenReturn(news);

		NewsTO returnedNews = nService.getNewsById(4L);

		assertNotNull(returnedNews);
		assertEquals(news, returnedNews);
		verify(newsDao).getNewsById(4L);
		verifyNoMoreInteractions(newsDao);
	}

	@Test
	public void testGetNewsByAuthor() throws Exception {
		NewsTO news = new NewsTO();
		news.setId(4L);
		news.setTitle("title");
		news.setShortText("bla-bla");
		news.setFullText("bla-bla-bla-bla-bla");
		news.setCreationDate(DateParseUtils
				.getTimestampDateFromString("2015-01-22 12:54:12.851"));
		news.setModificationDate(DateParseUtils
				.getSimpleDateFromString("2015-03-11"));
		List<NewsTO> list = new ArrayList<NewsTO>();
		list.add(news);
		when(newsDao.getNewsByAuthor(3L)).thenReturn(list);
		
		List<NewsTO> returnedList = nService.getNewsByAuthor(3L);
		
		assertNotNull(returnedList);
		assertEquals(1, returnedList.size());
		assertEquals(news, returnedList.get(0));
		verify(newsDao).getNewsByAuthor(3L);
		verifyNoMoreInteractions(newsDao);
	}

	@Test
	public void testGetNewsByTagsListOfLong() throws Exception {
		NewsTO news = new NewsTO();
		news.setId(4L);
		news.setTitle("title");
		news.setShortText("bla-bla");
		news.setFullText("bla-bla-bla-bla-bla");
		news.setCreationDate(DateParseUtils
				.getTimestampDateFromString("2015-01-22 12:54:12.851"));
		news.setModificationDate(DateParseUtils
				.getSimpleDateFromString("2015-03-11"));
		List<NewsTO> newsList = new ArrayList<NewsTO>();
		newsList.add(news);
		List<Long> longList = new ArrayList<Long>();
		longList.add(6L);
		when(newsDao.getNewsByTags(longList)).thenReturn(newsList);
		
		List<NewsTO> returnedList = nService.getNewsByTags(longList);
		
		assertNotNull(returnedList);
		assertEquals(1, returnedList.size());
		assertEquals(news, returnedList.get(0));
		verify(newsDao).getNewsByTags(longList);
		verifyNoMoreInteractions(newsDao);
	}

	@Test
	public void testGetNewsByTagsLongArray() throws Exception {
		NewsTO news = new NewsTO();
		news.setId(4L);
		news.setTitle("title");
		news.setShortText("bla-bla");
		news.setFullText("bla-bla-bla-bla-bla");
		news.setCreationDate(DateParseUtils
				.getTimestampDateFromString("2015-01-22 12:54:12.851"));
		news.setModificationDate(DateParseUtils
				.getSimpleDateFromString("2015-03-11"));
		List<NewsTO> newsList = new ArrayList<NewsTO>();
		newsList.add(news);
		List<Long> longList = new ArrayList<Long>();
		longList.add(6L);
		when(newsDao.getNewsByTags(longList)).thenReturn(newsList);
		
		List<NewsTO> returnedList = nService.getNewsByTags(6L);
		
		assertNotNull(returnedList);
		assertEquals(1, returnedList.size());
		assertEquals(news, returnedList.get(0));
		verify(newsDao).getNewsByTags(longList);
		verifyNoMoreInteractions(newsDao);
	}

	@Test
	public void testGetNews() throws Exception {
		NewsTO news = new NewsTO();
		news.setId(4L);
		news.setTitle("title");
		news.setShortText("bla-bla");
		news.setFullText("bla-bla-bla-bla-bla");
		news.setCreationDate(DateParseUtils
				.getTimestampDateFromString("2015-01-22 12:54:12.851"));
		news.setModificationDate(DateParseUtils
				.getSimpleDateFromString("2015-03-11"));
		List<NewsTO> newsList = new ArrayList<NewsTO>();
		newsList.add(news);
		when(newsDao.getNews(null, 5L, 20, 10)).thenReturn(newsList);
		
		List<NewsTO> returnedList = nService.getNews(null, 5L, 20, 10);
		
		assertNotNull(returnedList);
		assertEquals(1, returnedList.size());
		assertEquals(news, returnedList.get(0));
		verify(newsDao).getNews(null, 5L, 20, 10);
		verifyNoMoreInteractions(newsDao);
	}

	@Test
	public void testCreateNews() throws Exception {
		NewsTO news = new NewsTO();
		news.setId(4L);
		news.setTitle("title");
		news.setShortText("bla-bla");
		news.setFullText("bla-bla-bla-bla-bla");
		news.setCreationDate(DateParseUtils
				.getTimestampDateFromString("2015-01-22 12:54:12.851"));
		news.setModificationDate(DateParseUtils
				.getSimpleDateFromString("2015-03-11"));
		when(newsDao.createNews(news)).thenReturn(1042L);
		
		long returnedId = nService.createNews(news);
		
		assertEquals(1042L, returnedId);
		verify(newsDao).createNews(news);
		verifyNoMoreInteractions(newsDao);
	}

	@Test
	public void testUpdateNews() throws Exception {
		NewsTO news = new NewsTO();
		news.setId(4L);
		news.setTitle("title");
		news.setShortText("bla-bla");
		news.setFullText("bla-bla-bla-bla-bla");
		news.setCreationDate(DateParseUtils
				.getTimestampDateFromString("2015-01-22 12:54:12.851"));
		news.setModificationDate(DateParseUtils
				.getSimpleDateFromString("2015-03-11"));
		
		nService.updateNews(news);
		
		verify(newsDao).updateNews(news);
		verifyNoMoreInteractions(newsDao);
	}

	@Test
	public void testAddNewNews() throws Exception {
		NewsTO news = new NewsTO();
		news.setId(4L);
		news.setTitle("title");
		news.setShortText("bla-bla");
		news.setFullText("bla-bla-bla-bla-bla");
		news.setCreationDate(DateParseUtils
				.getTimestampDateFromString("2015-01-22 12:54:12.851"));
		news.setModificationDate(DateParseUtils
				.getSimpleDateFromString("2015-03-11"));
		when(newsDao.getNewsById(4L)).thenReturn(null);
		
		nService.addNews(news);
		
		verify(newsDao).getNewsById(4L);
		verify(newsDao).createNews(news);
		verifyNoMoreInteractions(newsDao);
	}
	
	@Test
	public void testAddExistingNews() throws Exception {
		NewsTO news = new NewsTO();
		news.setId(4L);
		news.setTitle("title");
		news.setShortText("bla-bla");
		news.setFullText("bla-bla-bla-bla-bla");
		news.setCreationDate(DateParseUtils
				.getTimestampDateFromString("2015-01-22 12:54:12.851"));
		news.setModificationDate(DateParseUtils
				.getSimpleDateFromString("2015-03-11"));
		when(newsDao.getNewsById(4L)).thenReturn(new NewsTO());
		
		nService.addNews(news);
		
		verify(newsDao).getNewsById(4L);
		verify(newsDao).updateNews(news);
		verifyNoMoreInteractions(newsDao);
	}

	@Test
	public void testDeleteNews() throws Exception {
		nService.deleteNews(16L);
		
		verify(newsDao).deleteNews(16L);
		verifyNoMoreInteractions(newsDao);
	}

}
