package com.epam.newsmanagement.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.epam.newsmanagement.dao.TagDao;
import com.epam.newsmanagement.entity.TagTO;
import com.epam.newsmanagement.service.impl.TagServiceImpl;

public class TagServiceTest {
	
	private TagServiceImpl tService;
	@Mock
	private TagDao tagDao;
	
	@Before
	public void beforeTest() {
		MockitoAnnotations.initMocks(this);
		tService = new TagServiceImpl();
		tService.setTagDao(tagDao);
	}

	@Test
	public void testGetTagById() throws Exception {
		TagTO tag = new TagTO();
		tag.setId(7L);
		tag.setName("tag7");
		when(tagDao.getTagById(7L)).thenReturn(tag);
		
		TagTO returnedTag = tService.getTagById(7L);
		
		assertNotNull(returnedTag);
		assertEquals(tag, returnedTag);
		verify(tagDao).getTagById(7L);
		verifyNoMoreInteractions(tagDao);
	}

	@Test
	public void testGetTagByName() throws Exception {
		TagTO tag = new TagTO();
		tag.setId(7L);
		tag.setName("tag7");
		when(tagDao.getTagByName("tag7")).thenReturn(tag);
		
		TagTO returnedTag = tService.getTagByName("tag7");
		
		assertNotNull(returnedTag);
		assertEquals(tag, returnedTag);
		verify(tagDao).getTagByName("tag7");
		verifyNoMoreInteractions(tagDao);
	}

	@Test
	public void testGetTagsByNews() throws Exception {
		TagTO tag = new TagTO();
		tag.setId(7L);
		tag.setName("tag7");
		List<TagTO> tagList = new ArrayList<TagTO>();
		tagList.add(tag);
		when(tagDao.getTagsByNews(4L)).thenReturn(tagList);
		
		List<TagTO> returnedTagList = tService.getTagsByNews(4L);
		
		assertNotNull(returnedTagList);
		assertEquals(1, returnedTagList.size());
		assertEquals(tag, returnedTagList.get(0));
		verify(tagDao).getTagsByNews(4L);
		verifyNoMoreInteractions(tagDao);
	}

	@Test
	public void testGetAllTags() throws Exception {
		TagTO tag = new TagTO();
		tag.setId(7L);
		tag.setName("tag7");
		List<TagTO> tagList = new ArrayList<TagTO>();
		tagList.add(tag);
		when(tagDao.getAllTags()).thenReturn(tagList);
		
		List<TagTO> returnedTagList = tService.getAllTags();
		
		assertNotNull(returnedTagList);
		assertEquals(1, returnedTagList.size());
		assertEquals(tag, returnedTagList.get(0));
		verify(tagDao).getAllTags();
		verifyNoMoreInteractions(tagDao);
	}

	@Test
	public void testCreateTag() throws Exception {
		TagTO tag = new TagTO();
		tag.setId(7L);
		tag.setName("tag7");
		when(tagDao.createTag(tag)).thenReturn(1004L);
		
		long returnedId = tService.createTag(tag);
		
		assertEquals(1004L, returnedId);
		verify(tagDao).createTag(tag);
		verifyNoMoreInteractions(tagDao);
	}

	@Test
	public void testUpdateTag() throws Exception {
		TagTO tag = new TagTO();
		tag.setId(7L);
		tag.setName("tag7");
		
		tService.updateTag(tag);
		
		verify(tagDao).updateTag(tag);
		verifyNoMoreInteractions(tagDao);
	}

	@Test
	public void testAddNewTag() throws Exception {
		TagTO tag = new TagTO();
		tag.setId(7L);
		tag.setName("tag7");
		when(tagDao.getTagById(7L)).thenReturn(null);
		
		tService.addTag(tag);
		
		verify(tagDao).getTagById(7L);
		verify(tagDao).createTag(tag);
		verifyNoMoreInteractions(tagDao);
	}
	
	@Test
	public void testAddExistingTag() throws Exception {
		TagTO tag = new TagTO();
		tag.setId(7L);
		tag.setName("tag7");
		when(tagDao.getTagById(7L)).thenReturn(new TagTO());
		
		tService.addTag(tag);
		
		verify(tagDao).getTagById(7L);
		verify(tagDao).updateTag(tag);
		verifyNoMoreInteractions(tagDao);
	}

	@Test
	public void testDeleteTag() throws Exception {
		tService.deleteTag(25L);
		
		verify(tagDao).deleteTag(25L);
		verifyNoMoreInteractions(tagDao);
	}

	@Test
	public void testCreateNewsTagRecord() throws Exception {
		tService.createNewsTagRecord(5L, 7L);
		
		verify(tagDao).createNewsTag(5L, 7L);
		verifyNoMoreInteractions(tagDao);
	}

	@Test
	public void testDeleteNewsTagRecord() throws Exception {
		tService.deleteNewsTagRecord(6L, 4L);
		
		verify(tagDao).deleteNewsTag(6L, 4L);
		verifyNoMoreInteractions(tagDao);
	}

	@Test
	public void testAddTagsToNews() throws Exception {
		List<Long> tagIdList = new ArrayList<Long>();
		tagIdList.add(6L);
		tagIdList.add(12L);
		tagIdList.add(15L);
		tagIdList.add(20L);
		
		tService.addTagsToNews(7L, tagIdList);
		
		verify(tagDao).createNewsTag(7L, 6L);
		verify(tagDao).createNewsTag(7L, 12L);
		verify(tagDao).createNewsTag(7L, 15L);
		verify(tagDao).createNewsTag(7L, 20L);
		verifyNoMoreInteractions(tagDao);
	}

	@Test
	public void testChangeNewsTags() throws Exception {
		List<Long> tagIdList = new ArrayList<Long>();
		tagIdList.add(6L);
		tagIdList.add(12L);
		tagIdList.add(15L);
		tagIdList.add(20L);
		
		List<TagTO> oldTagList = new ArrayList<TagTO>();
		for(int i = 55; i < 60; i++) {
			TagTO tag = new TagTO();
			tag.setId(i);
			tag.setName("tagName");
			oldTagList.add(tag);
		}
		when(tagDao.getTagsByNews(3L)).thenReturn(oldTagList);
		
		tService.changeNewsTags(3L, tagIdList);
		
		verify(tagDao).getTagsByNews(3L);
		
		verify(tagDao).deleteNewsTag(3L, 55L);
		verify(tagDao).deleteNewsTag(3L, 56L);
		verify(tagDao).deleteNewsTag(3L, 57L);
		verify(tagDao).deleteNewsTag(3L, 58L);
		verify(tagDao).deleteNewsTag(3L, 59L);
		
		verify(tagDao).createNewsTag(3L, 6L);
		verify(tagDao).createNewsTag(3L, 12L);
		verify(tagDao).createNewsTag(3L, 15L);
		verify(tagDao).createNewsTag(3L, 20L);
		verifyNoMoreInteractions(tagDao);
	}

}
