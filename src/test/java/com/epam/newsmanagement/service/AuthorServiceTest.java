package com.epam.newsmanagement.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.epam.newsmanagement.dal.dao.jdbc.DateParseUtils;
import com.epam.newsmanagement.dao.AuthorDao;
import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.service.impl.AuthorServiceImpl;

public class AuthorServiceTest {

	private AuthorServiceImpl aService;
	@Mock
	private AuthorDao authorDao;

	@Before
	public void beforeTest() {
		MockitoAnnotations.initMocks(this);
		aService = new AuthorServiceImpl();
		aService.setAuthorDao(authorDao);
	}

	@Test
	public void testGetAuthorById() throws Exception {
		AuthorTO author = new AuthorTO();
		author.setId(1L);
		author.setName("auth");
		author.setExpired(DateParseUtils
				.getTimestampDateFromString("2014-03-23 15:17:18.456"));
		when(authorDao.getAuthorById(1L)).thenReturn(author);
		
		AuthorTO returnedAuthor = aService.getAuthorById(1L);
		
		verify(authorDao).getAuthorById(1L);
		verifyNoMoreInteractions(authorDao);
		assertEquals(author, returnedAuthor);
	}

	@Test
	public void testGetAuthorByName() throws Exception {
		AuthorTO author = new AuthorTO();
		author.setId(1L);
		author.setName("auth");
		author.setExpired(DateParseUtils
				.getTimestampDateFromString("2014-03-23 15:17:18.456"));
		when(authorDao.getAuthorByName("auth")).thenReturn(author);
		
		AuthorTO returnedAuthor = aService.getAuthorByName("auth");
		
		verify(authorDao).getAuthorByName("auth");
		verifyNoMoreInteractions(authorDao);
		assertEquals(author, returnedAuthor);
	}

	@Test
	public void testGetAuthorByNews() throws Exception {
		AuthorTO author = new AuthorTO();
		author.setId(1L);
		author.setName("auth");
		author.setExpired(DateParseUtils
				.getTimestampDateFromString("2014-03-23 15:17:18.456"));
		when(authorDao.getAuthorByNews(5L)).thenReturn(author);
		
		AuthorTO returnedAuthor = aService.getAuthorByNews(5L);
		
		verify(authorDao).getAuthorByNews(5L);
		verifyNoMoreInteractions(authorDao);
		assertEquals(author, returnedAuthor);
	}

	@Test
	public void testGetAllAuthors() throws Exception {
		List<AuthorTO> list = new ArrayList<AuthorTO>();
		AuthorTO author = new AuthorTO();
		author.setId(1L);
		author.setName("auth");
		author.setExpired(DateParseUtils
				.getTimestampDateFromString("2014-03-23 15:17:18.456"));
		list.add(author);
		when(authorDao.getAllAuthors()).thenReturn(list);
		
		List<AuthorTO> returnedList = aService.getAllAuthors();
		
		verify(authorDao).getAllAuthors();
		verifyNoMoreInteractions(authorDao);
		assertEquals(returnedList, list);
	}

	@Test
	public void testCreateAuthor() throws Exception {
		AuthorTO author = new AuthorTO();
		author.setId(1L);
		author.setName("auth");
		author.setExpired(DateParseUtils
				.getTimestampDateFromString("2014-03-23 15:17:18.456"));
		when(authorDao.createAuthor(any(AuthorTO.class))).thenReturn(1005L);
		
		long id = authorDao.createAuthor(author);
		
		verify(authorDao).createAuthor(author);
		verifyNoMoreInteractions(authorDao);
		assertEquals(1005L, id);
	}

	@Test
	public void testUpdateAuthor() throws Exception {
		AuthorTO author = new AuthorTO();
		author.setId(1L);
		author.setName("auth");
		author.setExpired(DateParseUtils
				.getTimestampDateFromString("2014-03-23 15:17:18.456"));
		
		aService.updateAuthor(author);
		
		verify(authorDao).updateAuthor(author);
		verifyNoMoreInteractions(authorDao);
	}

	@Test
	public void testAddNewAuthor() throws Exception {
		AuthorTO author = new AuthorTO();
		author.setId(36L);
		author.setName("auth");
		author.setExpired(DateParseUtils
				.getTimestampDateFromString("2014-03-23 15:17:18.456"));
		when(authorDao.getAuthorById(36L)).thenReturn(null);
		
		aService.addAuthor(author);
		
		verify(authorDao).getAuthorById(36L);
		verify(authorDao).createAuthor(author);
		verifyNoMoreInteractions(authorDao);
	}
	
	@Test
	public void testAddExistingAuthor() throws Exception {
		AuthorTO author = new AuthorTO();
		author.setId(36L);
		author.setName("auth");
		author.setExpired(DateParseUtils
				.getTimestampDateFromString("2014-03-23 15:17:18.456"));
		when(authorDao.getAuthorById(36L)).thenReturn(new AuthorTO());
		
		aService.addAuthor(author);
		
		verify(authorDao).getAuthorById(36L);
		verify(authorDao).updateAuthor(author);
		verifyNoMoreInteractions(authorDao);
	}

	@Test
	public void testDeleteAuthor() throws Exception {
		aService.deleteAuthor(7L);
		
		verify(authorDao).deleteAuthor(7L);
		verifyNoMoreInteractions(authorDao);
	}

	@Test
	public void testCreateNewsAuthorRecord() throws Exception {
		aService.createNewsAuthorRecord(3L, 2L);
		
		verify(authorDao).createNewsAuthor(3L, 2L);
		verifyNoMoreInteractions(authorDao);
	}

	@Test
	public void testDeleteNewsAuthorRecord() throws Exception {
		aService.deleteNewsAuthorRecord(3L, 2L);
		
		verify(authorDao).deleteNewsAuthor(3L, 2L);
		verifyNoMoreInteractions(authorDao);
	}

	@Test
	public void testConnectAuthorAndNewsWithCurrent() throws Exception {
		AuthorTO author = new AuthorTO();
		author.setId(36L);
		author.setName("auth");
		author.setExpired(DateParseUtils
				.getTimestampDateFromString("2014-03-23 15:17:18.456"));
		when(authorDao.getAuthorByNews(17L)).thenReturn(author);
		
		aService.connectAuthorAndNews(4L, 17L);
		
		verify(authorDao).getAuthorByNews(17L);
		verify(authorDao).deleteNewsAuthor(17L, 36L);
		verify(authorDao).createNewsAuthor(17L, 4L);
		verifyNoMoreInteractions(authorDao);
	}
	
	@Test
	public void testConnectAuthorAndNewsWithoutCurrent() throws Exception {
		when(authorDao.getAuthorByNews(17L)).thenReturn(null);
		
		aService.connectAuthorAndNews(4L, 17L);
		
		verify(authorDao).getAuthorByNews(17L);
		verify(authorDao).createNewsAuthor(17L, 4L);
		verifyNoMoreInteractions(authorDao);
	}

}
