package com.epam.newsmanagement.dal.dao.jdbc;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

public class DateParseUtils {
	
	private static final Logger LOG = Logger.getLogger(DateParseUtils.class);
	
	public static Date getTimestampDateFromString(String str) {
		Date date = null;
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
		try {
			date = dateFormat.parse(str);
		} catch (ParseException e) {
			LOG.warn("check test data, this Exception must not happen!", e);
		}
		return date;
	}
	
	public static Date getSimpleDateFromString(String str) {
		Date date = null;
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		try {
			date = dateFormat.parse(str);
		} catch (ParseException e) {
			LOG.warn("check test data, this Exception must not happen!", e);
		}
		return date;
	}

}
