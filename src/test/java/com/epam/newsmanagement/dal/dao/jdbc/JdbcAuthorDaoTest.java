package com.epam.newsmanagement.dal.dao.jdbc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.newsmanagement.dao.AuthorDao;
import com.epam.newsmanagement.dao.NewsDao;
import com.epam.newsmanagement.entity.AuthorTO;
import com.epam.newsmanagement.entity.NewsTO;
import com.epam.newsmanagement.exception.DaoException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:BeansForTesting.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
@DatabaseSetup(value = "classpath:AuthorDaoTestData.xml")
public class JdbcAuthorDaoTest {

	@Autowired
	private AuthorDao authorDao;
	@Autowired
	private NewsDao newsDao;

	@Test
	public void testGetAuthorById() throws DaoException {
		AuthorTO author = authorDao.getAuthorById(2L);

		assertNotNull(author);
		assertEquals(2L, author.getId());
		assertEquals("author 2", author.getName());
		assertEquals(
				DateParseUtils.getTimestampDateFromString("2015-02-23 17:15:13.020"),
				author.getExpired());
	}

	@Test
	public void testGetAuthorByName() throws DaoException {
		AuthorTO author = authorDao.getAuthorByName("author 4");

		assertNotNull(author);
		assertEquals(4L, author.getId());
		assertEquals("author 4", author.getName());
		assertEquals(
				DateParseUtils.getTimestampDateFromString("2015-02-23 17:15:00.000"),
				author.getExpired());
	}

	@Test
	public void testGetAuthorByNews() throws DaoException {
		AuthorTO author = authorDao.getAuthorByNews(10L);

		assertNotNull(author);
		assertEquals(5L, author.getId());
		assertEquals("author 5", author.getName());
		assertEquals(
				DateParseUtils.getTimestampDateFromString("2015-02-23 17:15:00.000"),
				author.getExpired());
	}

	@Test
	public void testCreateAuthor() throws DaoException {
		AuthorTO author = new AuthorTO();
		author.setId(22L);
		author.setName("inserted author");
		author.setExpired(DateParseUtils
				.getTimestampDateFromString("2013-11-12 05:38:17.000"));

		long id = authorDao.createAuthor(author);

		AuthorTO insertedAuthor = authorDao.getAuthorById(id);
		assertNotNull(insertedAuthor);
		assertEquals(id, insertedAuthor.getId());
		assertEquals("inserted author", insertedAuthor.getName());
		assertEquals(
				DateParseUtils.getTimestampDateFromString("2013-11-12 05:38:17.000"),
				insertedAuthor.getExpired());
	}

	@Test
	public void testUpdateAuthor() throws DaoException {
		AuthorTO author = new AuthorTO();
		author.setId(15L);
		author.setName("updated author (15)");
		author.setExpired(DateParseUtils
				.getTimestampDateFromString("2012-01-01 00:01:13.666"));

		authorDao.updateAuthor(author);

		AuthorTO updatedAuthor = authorDao.getAuthorById(15L);
		assertNotNull(updatedAuthor);
		assertEquals(15L, updatedAuthor.getId());
		assertEquals("updated author (15)", updatedAuthor.getName());
		assertEquals(
				DateParseUtils.getTimestampDateFromString("2012-01-01 00:01:13.666"),
				updatedAuthor.getExpired());
	}

	@Test
	public void testDeleteAuthor() throws DaoException {
		authorDao.deleteAuthor(11L);

		AuthorTO deletedAuthor = authorDao.getAuthorById(11L);
		assertNull(deletedAuthor);
	}

	@Test
	public void testCreateNewsAuthor() throws DaoException {
		authorDao.createNewsAuthor(15L, 2L);
		List<NewsTO> list = newsDao.getNewsByAuthor(2L);

		boolean reallyCreated = false;
		for (NewsTO news : list) {
			if (news.getId() == 15L) {
				reallyCreated = true;
			}
		}
		assertTrue(reallyCreated);
	}

	@Test
	public void testDeleteNewsAuthor() throws DaoException {
		authorDao.deleteNewsAuthor(4L, 13L);
		List<NewsTO> list = newsDao.getNewsByAuthor(13L);
		for (NewsTO news : list) {
			assertFalse(news.getId() == 4L);
		}
	}

}
