package com.epam.newsmanagement.dal.dao.jdbc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.newsmanagement.dao.CommentDao;
import com.epam.newsmanagement.entity.CommentTO;
import com.epam.newsmanagement.exception.DaoException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:BeansForTesting.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
@DatabaseSetup(value = "classpath:CommentDaoTestData.xml")
public class JdbcCommentDaoTest {

	@Autowired
	CommentDao commentDao;

	@Test
	public void testGetCommentById() throws DaoException {
		CommentTO comment = commentDao.getCommentById(3L);

		assertNotNull(comment);
		assertEquals(3L, comment.getId());
		assertEquals("text 3", comment.getText());
		assertEquals(1L, comment.getNewsId());
		assertEquals(
				DateParseUtils
						.getTimestampDateFromString("2015-02-25 17:15:00.000"),
				comment.getCreationDate());
	}

	@Test
	public void testGetCommentsByNews() throws DaoException {
		List<CommentTO> list = commentDao.getCommentsByNews(4L);

		assertNotNull(list);
		assertEquals(2, list.size());

		CommentTO comment1 = commentDao.getCommentById(23L);
		CommentTO comment2 = commentDao.getCommentById(25L);
		assertTrue(list.contains(comment1));
		assertTrue(list.contains(comment2));
	}

	@Test
	public void testCreateComment() throws DaoException {
		CommentTO comment = new CommentTO();
		comment.setId(70L);
		comment.setText("inserted comment");
		comment.setNewsId(8L);
		comment.setCreationDate(DateParseUtils
				.getTimestampDateFromString("2014-05-28 14:15:00.000"));

		long id = commentDao.createComment(comment);

		CommentTO newComment = commentDao.getCommentById(id);
		assertNotNull(newComment);
		assertEquals(id, newComment.getId());
		assertEquals("inserted comment", newComment.getText());
		assertEquals(8L, newComment.getNewsId());
		assertEquals(
				DateParseUtils
						.getTimestampDateFromString("2014-05-28 14:15:00.000"),
				newComment.getCreationDate());
	}

	@Test
	public void testUpdateComment() throws DaoException {
		CommentTO comment = new CommentTO();
		comment.setId(10L);
		comment.setText("updated comment");
		comment.setNewsId(8L);
		comment.setCreationDate(DateParseUtils
				.getTimestampDateFromString("2014-05-28 14:15:00.027"));

		commentDao.updateComment(comment);

		CommentTO updatedComment = commentDao.getCommentById(10L);
		assertNotNull(updatedComment);
		assertEquals(10L, updatedComment.getId());
		assertEquals("updated comment", updatedComment.getText());
		assertEquals(8L, updatedComment.getNewsId());
		assertEquals(
				DateParseUtils
						.getTimestampDateFromString("2014-05-28 14:15:00.027"),
				updatedComment.getCreationDate());
	}

	@Test
	public void testDeleteComment() throws DaoException {
		commentDao.deleteComment(2L);

		CommentTO deletedComment = commentDao.getCommentById(2L);
		assertNull(deletedComment);
	}

}
