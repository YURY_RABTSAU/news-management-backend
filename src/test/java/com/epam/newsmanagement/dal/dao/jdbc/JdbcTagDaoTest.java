package com.epam.newsmanagement.dal.dao.jdbc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.epam.newsmanagement.dao.TagDao;
import com.epam.newsmanagement.entity.TagTO;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:BeansForTesting.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
@DatabaseSetup(value = "classpath:TagDaoTestData.xml")
public class JdbcTagDaoTest {

	@Autowired
	private TagDao tagDao;

	@Test
	public void testGetTagById() throws Exception {
		TagTO tag = tagDao.getTagById(1L);

		assertNotNull(tag);
		assertEquals(1L, tag.getId());
		assertEquals("tag name 1", tag.getName());
	}

	@Test
	public void testGetTagByName() throws Exception {
		TagTO tag = tagDao.getTagByName("tag name 2");

		assertNotNull(tag);
		assertEquals(2L, tag.getId());
		assertEquals("tag name 2", tag.getName());
	}

	@Test
	public void testGetTagsByNews() throws Exception {
		List<TagTO> res = new ArrayList<TagTO>();

		res = tagDao.getTagsByNews(2L);

		assertNotNull(res);
		assertEquals(2, res.size());

		TagTO tag1 = tagDao.getTagById(1L);
		TagTO tag2 = tagDao.getTagById(3L);
		assertTrue(res.contains(tag1));
		assertTrue(res.contains(tag2));

	}

	@Test
	public void testCreateTag() throws Exception {
		TagTO newTag = new TagTO();
		newTag.setId(4L);
		newTag.setName("inserted tag");

		long id = tagDao.createTag(newTag);

		TagTO insertedTag = tagDao.getTagById(id);
		assertNotNull(insertedTag);
		assertEquals(id, insertedTag.getId());
		assertEquals("inserted tag", insertedTag.getName());
	}

	@Test
	public void testUpdateTag() throws Exception {
		TagTO newTag = new TagTO();
		newTag.setId(3L);
		newTag.setName("updated tag (3)");

		tagDao.updateTag(newTag);

		TagTO updatedTag = tagDao.getTagById(3L);
		assertNotNull(updatedTag);
		assertEquals(3L, updatedTag.getId());
		assertEquals("updated tag (3)", updatedTag.getName());
	}

	@Test
	public void testDeleteTag() throws Exception {
		tagDao.deleteTag(1L);

		TagTO deletedTag = tagDao.getTagById(1L);
		assertNull(deletedTag);
	}

	@Test
	public void testCreateNewsTag() throws Exception {
		tagDao.createNewsTag(2L, 2L);

		List<TagTO> list = tagDao.getTagsByNews(2L);
		TagTO tag = tagDao.getTagById(2L);
		assertTrue(list.contains(tag));
	}

	@Test
	public void testDeleteNewsTag() throws Exception {

		tagDao.deleteNewsTag(3L, 3L);

		List<TagTO> list = tagDao.getTagsByNews(3L);
		TagTO tag = tagDao.getTagById(3L);
		assertFalse(list.contains(tag));
	}

}
